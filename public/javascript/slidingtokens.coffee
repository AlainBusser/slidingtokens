origine = $($("#graphe tr").get(0).cells[0].closest("td"))
destination = $($("#graphe tr").get(0).cells[0].closest("td"))
$ ->
    $("#graphe").draggable()
    place = (x,y) ->
        $($("#graphe tr").get(y).cells[x]).closest("td")
    occupé = (sommet) ->
        (sommet.hasClass "rouge") or (sommet.hasClass "bleu")
    vide = (sommet) ->
        not occupé(sommet)
    gaucheocc = (sommet) ->
        xs = sommet.closest("td").index()
        ys = sommet.closest("tr").index()
        place(xs-1,ys).hasClass("est") and occupé(place(xs-2,ys))
    droiteocc = (sommet) ->
        xs = sommet.closest("td").index()
        ys = sommet.closest("tr").index()
        place(xs+1,ys).hasClass("est") and occupé(place(xs+2,ys))
    hautocc = (sommet) ->
        xs = sommet.closest("td").index()
        ys = sommet.closest("tr").index()
        place(xs,ys-1).hasClass("nord") and occupé(place(xs,ys-2))
    basocc = (sommet) ->
        xs = sommet.closest("td").index()
        ys = sommet.closest("tr").index()
        place(xs,ys+1).hasClass("nord") and occupé(place(xs,ys+2))
    possible = (sommet) ->
        v = 0
        if gaucheocc(sommet)
            v += 1
        if droiteocc(sommet)
            v += 1
        if basocc(sommet)
            v += 1
        if hautocc(sommet)
            v += 1
        v == 1
    unCoup =  () ->
                if occupé(destination)
                    [origine,destination] = [destination,origine]
                if (origine.hasClass "rouge") and possible(destination)
                    origine.removeClass "rouge"
                    destination.addClass "rouge"
                if (origine.hasClass "bleu") and possible(destination)
                    origine.removeClass "bleu"
                    destination.addClass "bleu"
                if destination.hasClass "rose"
                    alert "Bravo, la licorne est à la maison !"
    $(".nord").on 'click', ->
        j = $(this).closest("tr").index()
        i = $(this).closest("td").index()
        origine = place(i,j-1)
        destination = place(i,j+1)
        unCoup()
    $(".est").on 'click', ->
        j = $(this).closest("tr").index()
        i = $(this).closest("td").index()
        origine = place(i-1,j)
        destination = place(i+1,j)
        unCoup()
 
