# slidingtokens

one-player game on a graph

Game invented by E. Demaine around year 2000

One of the nodes of the graph is the :unicorn: ' home. Help :unicorn: to come back to its home !

But, two nodes being said *adjacent* if there is an arc between them,

*  Two :horse: should *never* be adjacent. Never !
*  The :unicorn: and a :horse: should never be adjacent either
*  Any node is not large enough to shelter more than one :horse: or :unicorn:

[The game is here](public/html/grid1.html)
